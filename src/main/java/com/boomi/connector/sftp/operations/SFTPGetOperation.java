//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.operations;

import com.boomi.connector.api.GetRequest;
import com.boomi.connector.api.OperationResponse;
import com.boomi.connector.api.ResponseUtil;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.handlers.DownloadHandler;
import com.boomi.connector.util.BaseGetOperation;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class SFTPGetOperation extends BaseGetOperation {

	public SFTPGetOperation(SFTPConnection conn) {
		super(conn);
	}

	@Override
	protected void executeGet(GetRequest request, OperationResponse response) {

		try {
			this.getConnection().openConnection();
			DownloadHandler handler = new DownloadHandler(this.getConnection(), response);
			handler.processInput(request.getObjectId());
		} catch (Exception e) {
			ResponseUtil.addExceptionFailure(response, request.getObjectId(), e);
		} finally {
			this.getConnection().closeConnection();
		}
	}

	@Override
	public SFTPConnection getConnection() {
		return (SFTPConnection) super.getConnection();
	}
}
