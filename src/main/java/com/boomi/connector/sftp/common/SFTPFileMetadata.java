//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.common;

 /**
  * @author Omesh Deoli
  *
  * ${tags}
  */
import com.boomi.connector.api.Payload;
import com.boomi.connector.api.PayloadMetadata;
import com.boomi.connector.api.PayloadMetadataFactory;

public interface SFTPFileMetadata {
	public String getDirectory();

	public String getName();

	public PayloadMetadata toPayloadMetadata(PayloadMetadataFactory var1);

	public Payload toJsonPayload();
}
