//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.common;

 /**
  * @author Omesh Deoli
  *
  * ${tags}
  */
public interface PathsHandler {
	public SFTPFileMetadata splitIntoDirAndFileName(String var1);

	public String resolvePaths(String var1, String var2);

	public SFTPFileMetadata splitIntoDirAndFileName(String var1, String var2);

	public String joinPaths(String var1, String var2);

	public boolean isFullPath(String var1);
}
