//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.TrackedData;

import com.boomi.connector.sftp.SFTPConnection;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class RetryableVerifyFileExistsAction extends SingleRetryAction {
	private String fullPath;
	private Boolean fileExists;

	public Boolean getFileExists() {
		return fileExists;
	}

	public void setFileExists(Boolean fileExists) {
		this.fileExists = fileExists;
	}

	public RetryableVerifyFileExistsAction(SFTPConnection connection, String remoteDir, String fileName,
			TrackedData input) {
		super(connection, remoteDir, input);

	}

	public RetryableVerifyFileExistsAction(SFTPConnection connection, String fullPath,TrackedData input) {
		super(connection, null, input);
		this.fullPath = fullPath;
	}

	public RetryableVerifyFileExistsAction(SFTPConnection connection, String remoteDir, String fileName) {
		super(connection, null, null);
		this.fullPath = connection.getPathsHandler().joinPaths(remoteDir, fileName);
	}

	@Override
	public void doExecute() {
		fileExists = this.getConnection().fileExists(fullPath);
	}

}
