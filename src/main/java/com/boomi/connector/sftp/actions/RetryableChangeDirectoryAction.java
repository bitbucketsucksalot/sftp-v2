//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.TrackedData;

import com.boomi.connector.sftp.SFTPConnection;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class RetryableChangeDirectoryAction extends SingleRetryAction {
	private String fullPath;
	
	public RetryableChangeDirectoryAction(SFTPConnection connection, String remoteDir, String fileName,
			TrackedData input) {
		super(connection, remoteDir, input);

	}

	public RetryableChangeDirectoryAction(SFTPConnection connection, String fullPath,TrackedData input) {
		super(connection, fullPath, input);
		this.fullPath = fullPath;
	}

	@Override
	public void doExecute() {
		this.getConnection().changeCurrentDirectory(fullPath);
	}

}
