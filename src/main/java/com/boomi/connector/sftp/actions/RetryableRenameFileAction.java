//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.SFTPConnection;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public class RetryableRenameFileAction extends SingleRetryAction {
	private final String originalPath;
	private final String newPath;

	public RetryableRenameFileAction(SFTPConnection connection, String originalPath, String newPath,TrackedData input) {
		super(connection, null, input);
		this.originalPath = originalPath;
		this.newPath = newPath;
	}

	@Override
	public void doExecute() {
		this.getConnection().renameFile(this.originalPath, this.newPath);
	}
}
