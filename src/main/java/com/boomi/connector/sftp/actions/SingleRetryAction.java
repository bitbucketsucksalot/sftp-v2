//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp.actions;

import com.boomi.connector.api.TrackedData;
import com.boomi.connector.sftp.SFTPConnection;
import com.boomi.connector.sftp.retry.RetryStrategyFactory;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
abstract class SingleRetryAction extends RetryableAction {
	protected static final RetryStrategyFactory SINGLE_RETRY_FACTORY = RetryStrategyFactory.createFactory(1);

	SingleRetryAction(SFTPConnection connection, String remoteDir, TrackedData input) {
		super(connection, remoteDir, input, SINGLE_RETRY_FACTORY);
	}
}
