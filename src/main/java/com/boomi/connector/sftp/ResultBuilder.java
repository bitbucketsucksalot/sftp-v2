//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

import java.io.IOException;

import com.boomi.connector.sftp.actions.RetryableQueryAction;
import com.boomi.connector.sftp.results.BaseResult;
import com.jcraft.jsch.ChannelSftp.LsEntry;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public interface ResultBuilder {

			public BaseResult makeResult(LsEntry var2,String dirFullPath ,RetryableQueryAction action) throws IOException;
	
}
