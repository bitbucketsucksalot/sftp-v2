//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

import com.boomi.connector.sftp.actions.RetryableQueryAction;
import com.boomi.connector.sftp.handlers.QueryHandler;
import com.boomi.connector.sftp.results.BaseResult;
import com.jcraft.jsch.ChannelSftp.LsEntry;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class ListResultBuilder implements ResultBuilder {

	@Override
	public BaseResult makeResult(LsEntry meta,String dirFullPath , RetryableQueryAction action) {
		
		return new BaseResult(QueryHandler.makeJsonPayload(meta,dirFullPath));
	}
	
}
