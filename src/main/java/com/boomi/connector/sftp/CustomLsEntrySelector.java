//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

import java.io.File;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.OperationStatus;

import com.boomi.connector.sftp.actions.RetryableQueryAction;

import com.boomi.connector.sftp.common.FileMetadata;
import com.boomi.connector.sftp.common.UnixPathsHandler;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.sftp.results.BaseResult;
import com.boomi.connector.sftp.results.ErrorResult;
import com.boomi.connector.sftp.results.MultiResult;
import com.boomi.connector.sftp.retry.RetryStrategyFactory;
import com.boomi.util.IOUtil;
import com.boomi.util.LogUtil;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.ChannelSftp.LsEntrySelector;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class CustomLsEntrySelector implements LsEntrySelector {

	BaseResult partialResult;

	MultiResult result;
	FileQueryFilter filter;
	long limit;
	Logger logger;
	ResultBuilder resultBuilder;
	String dirFullPath;
	SFTPConnection conn;
	UnixPathsHandler pathshandler = new UnixPathsHandler();
	SFTPClient newClient;
	int numberOfEntriesProcessed;
	private boolean reconnectFailed;

	public boolean isReconnectFailed() {
		return reconnectFailed;
	}

	public void setReconnectFailed(boolean reconnectFailed) {
		this.reconnectFailed = reconnectFailed;
	}


	@Override
	public int select(LsEntry entry) {
		try {

			final String filename = entry.getFilename();

			if (filename.equals(".") || filename.equals("..")) {
				numberOfEntriesProcessed++;
				return CONTINUE;
			}

			if (!entry.getAttrs().isLink()) {
				String fullFilePath = pathshandler.joinPaths(dirFullPath, entry.getFilename());
				File fullFilePathObj = new File(fullFilePath);
				partialResult = getPartialResult(filter, logger, fullFilePathObj.toPath(), resultBuilder, entry);
				numberOfEntriesProcessed++;
				if (partialResult != null) {
					if(!(partialResult instanceof ErrorResult)) {
					if (logger.isLoggable(Level.FINE)) {
						LogUtil.fine((Logger) logger, "Adding %s", (Object[]) new Object[] { fullFilePath });
					}
					result.addPartialResult(dirFullPath, entry.getFilename(), partialResult);
					}
					
				}
				if ((long) result.getSize() == limit) {
					return BREAK;
				}
			}

			return CONTINUE;
		} finally {
			if (partialResult != null && resultBuilder instanceof QueryResultBuilder) {
				QueryResultBuilder queryResult = (QueryResultBuilder) resultBuilder;
				IOUtil.closeQuietly(queryResult.getFilecontent());
			}
		}
	}

	public CustomLsEntrySelector(MultiResult result, FileQueryFilter filter, long limit, Logger logger,
			ResultBuilder resultBuilder, String dirFullPath, SFTPConnection conn, SFTPClient newClient) {
		this.result = result;
		this.filter = filter;
		this.limit = limit;
		this.logger = logger;
		this.resultBuilder = resultBuilder;
		this.newClient = newClient;
		this.dirFullPath = dirFullPath;
		this.conn = conn;
	}

	private BaseResult getPartialResult(FileQueryFilter filter, Logger logger, Path path, ResultBuilder resultBuilder,
			LsEntry entry) {
		RetryableQueryAction rQuery = null;
		try {

			if (filter.accept(entry)) {
				rQuery = new RetryableQueryAction(conn, dirFullPath, filter.getInput(),
						RetryStrategyFactory.createFactory(1), newClient,
						FileMetadata.joinPaths(dirFullPath, entry.getFilename()));
				if (resultBuilder instanceof QueryResultBuilder) {
					if (!reconnectFailed) {
						return resultBuilder.makeResult(entry, dirFullPath, rQuery);
					} else {
						LogUtil.warning((Logger) logger, "Error fetching file from " +FileMetadata.joinPaths(dirFullPath, entry.getFilename()), new ConnectorException("Lost Connectivity to Remote System"));
						return new ErrorResult(OperationStatus.APPLICATION_ERROR,
								new ConnectorException("Lost Connectivity to Remote System"));
					}
				} else {
					return resultBuilder.makeResult(entry, dirFullPath, rQuery);
				}
			}
			return null;
		} catch (Exception e) {
			if (rQuery != null) {
				reconnectFailed = rQuery.isReconnectFailed();
				if (reconnectFailed) {
					LogUtil.warning((Logger) logger, "Error fetching file from " +FileMetadata.joinPaths(dirFullPath, entry.getFilename()),e);
					return new ErrorResult(OperationStatus.APPLICATION_ERROR, e);
				}
			}
			LogUtil.warning((Logger) logger, e, SFTPConstants.UNEXPECTED_ERROR_OCCURED,
					(Object[]) new Object[] { path });
			return new ErrorResult(OperationStatus.APPLICATION_ERROR, e);
		} finally {
			if (rQuery != null) {
				rQuery.close();
			}
		}
	}

}
