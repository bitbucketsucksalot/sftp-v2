//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

import com.boomi.connector.api.FilterData;
import com.jcraft.jsch.ChannelSftp.LsEntry;

import java.io.File;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class FileExclusiveQueryFilter extends FileQueryFilter {
	public FileExclusiveQueryFilter(File directory, FilterData input, String dirPath) {
		super(directory, input, dirPath);
	}

	@Override
	public boolean accept(LsEntry entry) {
		return (!entry.getAttrs().isLink()) && (!entry.getAttrs().isDir()) && super.accept(entry);
	}

}
