//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.util.json.JSONUtil;
import java.io.IOException;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public enum SFTPObject {
	FILE("File"), FILE_CREATE_UPSERT("File", null, "file-schema.json"),
	DIRECTORY("Directory", null, "file-schema.json");

	private final String _label;
	private final String _outSchema;
	private final String _inSchema;

	private SFTPObject(String label) {
		this(label, null, null);
	}

	private SFTPObject(String label, String inSchema, String outSchema) {
		this._label = label;
		this._outSchema = outSchema;
		this._inSchema = inSchema;
	}

	public String getLabel() {
		return this._label;
	}

	public ObjectDefinition define(ObjectDefinitionRole role) {
		ObjectDefinition def;
		String schema = this.getSchema(role);
		if (schema == null) {
			def = new ObjectDefinition();
			def.setElementName("");
			if (role == ObjectDefinitionRole.INPUT) {
				def.withInputType(ContentType.BINARY).withOutputType(ContentType.NONE);
			} else {
				def.withInputType(ContentType.NONE).withOutputType(ContentType.BINARY);
			}
		} else {
			try {
				def = JSONUtil.newJsonDefinitionFromResource((ObjectDefinitionRole) role, (String) schema);
			} catch (IOException e) {
				throw new ConnectorException(SFTPConstants.ERROR_CREATING_JSON_DEFINITION, (Throwable) e);
			}
		}
		return def;
	}

	public ObjectType makeObjectType() {
		return new ObjectType().withId(this.name()).withLabel(this.getLabel());
	}

	private String getSchema(ObjectDefinitionRole role) {
		switch (role) {
		case INPUT: {
			return this._inSchema;
		}
		case OUTPUT: {
			return this._outSchema;
		}
		}
		throw new IllegalArgumentException(SFTPConstants.UNKNOWN_DEFINITION_ROLE + (Object) role);
	}

}
