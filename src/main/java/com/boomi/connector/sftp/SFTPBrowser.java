//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.boomi.connector.api.ConnectionTester;
import com.boomi.connector.api.ConnectorException;
import com.boomi.connector.api.ContentType;
import com.boomi.connector.api.ObjectDefinition;
import com.boomi.connector.api.ObjectDefinitionRole;
import com.boomi.connector.api.ObjectDefinitions;
import com.boomi.connector.api.ObjectType;
import com.boomi.connector.api.ObjectTypes;
import com.boomi.connector.sftp.constants.SFTPConstants;
import com.boomi.connector.api.FieldSpecField;
import com.boomi.connector.util.BaseBrowser;
import com.boomi.util.json.JSONUtil;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @author Omesh Deoli
 *
 *         ${tags}
 */
public class SFTPBrowser extends BaseBrowser implements ConnectionTester {

	protected SFTPBrowser(SFTPConnection conn) {
		super(conn);
	}

	private static final List<FieldSpecField> QUERY_FIELDS = SFTPBrowser.initQueryFields();

	private static final List<FieldSpecField> LIST_FIELDS = SFTPBrowser.initListFields();

	@Override
	public ObjectDefinitions getObjectDefinitions(String objectTypeId, Collection<ObjectDefinitionRole> roles) {
		ObjectDefinitions objectDefinitions = new ObjectDefinitions();
		for (ObjectDefinitionRole role : roles) {
			objectDefinitions.withDefinitions(this.buildObjectDefinition(role));
		}
		return objectDefinitions;

	}

	private ObjectDefinition buildObjectDefinition(ObjectDefinitionRole role) {
		ObjectDefinition objectDefinition = new ObjectDefinition();
		switch (this.getContext().getOperationType()) {
		case QUERY: {

			if (this.isListOperation()) {
				return objectDefinition.withInputType(ContentType.NONE)
						.withOutputType(role == ObjectDefinitionRole.OUTPUT ? ContentType.BINARY : ContentType.NONE)
						.withFieldSpecFields(LIST_FIELDS);
			} else {
				return objectDefinition.withInputType(ContentType.NONE)
						.withOutputType(role == ObjectDefinitionRole.OUTPUT ? ContentType.BINARY : ContentType.NONE)
						.withFieldSpecFields(QUERY_FIELDS);
			}

		}
		case UPSERT:
		case CREATE: {
			return this.prepareCreateObjDefs(role);
		}
		case DELETE: {
			return objectDefinition.withInputType(ContentType.JSON).withOutputType(ContentType.NONE);
		}
		case GET: {
			return objectDefinition.withInputType(ContentType.NONE).withOutputType(ContentType.BINARY);
		}
		default:
			throw new UnsupportedOperationException();
		}

	}

	private ObjectDefinition prepareCreateObjDefs(ObjectDefinitionRole role) {
		ObjectDefinition objectDefinition = new ObjectDefinition()
				.withInputType(role == ObjectDefinitionRole.INPUT ? ContentType.BINARY : ContentType.NONE)
				.withOutputType(role == ObjectDefinitionRole.OUTPUT ? ContentType.JSON : ContentType.NONE);
		if (role == ObjectDefinitionRole.OUTPUT) {
			boolean includeAllMetadata = this.getContext().getOperationProperties()
					.getBooleanProperty(SFTPConstants.PROPERTY_INCLUDE_METADATA, Boolean.FALSE);
			String outSchemaPath = includeAllMetadata ? SFTPConstants.EXTENDED_FILE_META_SCHEMA_PATH
					: SFTPConstants.SIMPLE_FILE_META_SCHEMA_PATH;
			ObjectNode jsonCookie = JSONUtil.newObjectNode().put(SFTPConstants.PROPERTY_INCLUDE_METADATA,
					includeAllMetadata);
			objectDefinition.withCookie(jsonCookie.toString()).withJsonSchema(SFTPBrowser.getSchema(outSchemaPath))
					.withElementName("");
		}
		return objectDefinition;
	}

	private static String getSchema(String outSchemaPath) {
		try {
			return JSONUtil.loadSchemaFromResource((String) outSchemaPath).toString();
		} catch (IOException e) {
			throw new ConnectorException(MessageFormat.format(SFTPConstants.ERROR_SCHEMA_LOAD_FORMAT, outSchemaPath),
					(Throwable) e);
		}
	}

	@Override
	public ObjectTypes getObjectTypes() {
		ObjectTypes types = new ObjectTypes();
		List<ObjectType> list = types.getTypes();
		switch (this.getContext().getOperationType()) {

		case QUERY: {
			list.add(SFTPCustomType.valueOf(this.getContext().getCustomOperationType()).getObject().makeObjectType());
			return types;
		}
		case UPSERT:
		case GET:
		case CREATE:
		case DELETE: {
			ObjectType objectType = new ObjectType().withId(SFTPConstants.OBJECT_TYPE_FILE)
					.withLabel(SFTPConstants.OBJECT_TYPE_FILE);
			return types.withTypes(objectType);
		}
		default:
			throw new UnsupportedOperationException();
		}

	}

	private static List<FieldSpecField> initQueryFields() {
		String[] props = new String[] { SFTPConstants.FILESIZE, SFTPConstants.MODIFIED_DATE };
		ArrayList<FieldSpecField> fields = new ArrayList<FieldSpecField>();
		fields.add(new FieldSpecField().withName(SFTPConstants.PROPERTY_FILENAME).withFilterable(Boolean.valueOf(true))
				.withType(SFTPConstants.PATH_TYPE));
		for (String prop : props) {
			fields.add(new FieldSpecField().withName(prop).withFilterable(Boolean.valueOf(true))
					.withType(SFTPConstants.COMPARABLE_TYPE));
		}
		return fields;
	}

	private boolean isListOperation() {
		return SFTPCustomType.LIST.name().equals(this.getContext().getCustomOperationType());
	}

	private static List<FieldSpecField> initListFields() {
		List<FieldSpecField> fields = SFTPBrowser.initQueryFields();
		fields.add(new FieldSpecField().withName(SFTPConstants.IS_DIRECTORY).withFilterable(Boolean.valueOf(true))
				.withType(SFTPConstants.BOOLEAN_TYPE));
		return fields;
	}

	@Override
	public SFTPConnection getConnection() {
		return (SFTPConnection) super.getConnection();
	}

	@Override
	public void testConnection() {
		getConnection().testConnection();
	}
}
