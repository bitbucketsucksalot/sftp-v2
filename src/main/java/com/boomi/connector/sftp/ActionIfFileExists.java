//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public enum ActionIfFileExists {

	ERROR, FORCE_UNIQUE_NAMES, OVERWRITE, APPEND;
}
