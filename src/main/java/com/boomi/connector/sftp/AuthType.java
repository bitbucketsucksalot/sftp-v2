//Copyright (c) 2020 Boomi, Inc.

package com.boomi.connector.sftp;

/**
 * @author Omesh Deoli
 *
 * ${tags}
 */
public enum AuthType {

	PASSWORD("Username and Password"), PUBLIC_KEY("Using public Key");

	private String authTypeDesc;

	AuthType(String p) {
		authTypeDesc = p;
	}

	public String getAuthType() {
		return authTypeDesc;
	}
}
